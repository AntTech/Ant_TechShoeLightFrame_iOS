#
# Be sure to run `pod lib lint XQSDKFrame.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ShoeLightFrame'
  s.version          = '1.0.0'
  s.summary          = '智能鞋灯iOS SDK 框架'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://git.oschina.net/AntTech/Ant_TechShoeLightFrame_iOS'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '包月兴' => 'byx19870908@yeah.net' }
  s.source           = { :git => 'https://git.oschina.net/AntTech/Ant_TechShoeLightFrame_iOS.git', :tag => '1.0.0'}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '7.0'

  s.source_files ='ShoeLightFrame/Classes/include/*.{h}'
  s.vendored_libraries ='ShoeLightFrame/Classes/lib/*.{a}'
  
  # s.resource_bundles = {
  #   'XQSDKFrame' => ['XQSDKFrame/Assets/*.png']
  # }

  #s.public_header_files = 'XQSDKFrame/Classes/*.h'
end
