//
//  NSTimer+ANTBlockSupport.h
//  ShoeLight
//
//  Created by 包月兴 on 2016/11/4.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (ANTBlockSupport)
+(NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval
                                     block:(void(^)())block
                                   repeats:(BOOL)repeat;
@end
