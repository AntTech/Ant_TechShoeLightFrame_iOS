//
//  ANTDeviceViewController.m
//  ShoeLight
//
//  Created by 包月兴 on 2016/11/9.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import "ANTDeviceViewController.h"
#import "AKTKit.h"
#import "ANTBleFrame.h"
@interface ANTDeviceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,copy) UITableView *tableView;
@property (nonatomic,copy) UINavigationItem *navTitle;
@property (nonatomic,copy) UIBarButtonItem *searchItem;
@property (nonatomic,copy) NSArray *data;
@end

@implementation ANTDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _data = [NSArray array];
    self.view.backgroundColor = mAKT_Color_White;
    UINavigationBar *nav = [[UINavigationBar alloc] initWithFrame:CGRectMake(0,20,mAKT_SCREENWITTH ,44)];
   _navTitle = [[UINavigationItem alloc] initWithTitle:@"设备"];
    [nav pushNavigationItem:_navTitle animated:YES];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    _navTitle.leftBarButtonItem = leftItem;
    
    
    _searchItem = self.searchItem;
    [self.view addSubview:nav];
    _tableView = self.tableView;
}


-(UIBarButtonItem *)searchItem
{
    if (_searchItem == nil) {
        _searchItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchBle)];
        _navTitle.rightBarButtonItem = _searchItem;
    }
    return _searchItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchBle
{
    _navTitle.rightBarButtonItem = nil;
    _searchItem = nil;
    [ANTBleFrameInstance searchTwoPeripheralWithDuration:5 success:^(NSArray *peripherals) {
        _searchItem = self.searchItem;
        NSArray *arr = [peripherals sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSNumber *rssi1 = obj1[@"RSSI"];
            NSNumber *rssi2 = obj2[@"RSSI"];
            NSComparisonResult result = [rssi1 compare:rssi2];
            return result == NSOrderedAscending;
        }];
        
        _data = [NSArray arrayWithArray:arr];
        [self.tableView reloadData];
    } failure:^(ANTErorr *error) {
        _searchItem = self.searchItem;
    }];
}

-(UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,64,mAKT_SCREENWITTH, mAKT_SCREENHEIGHT) style:UITableViewStylePlain];
        [self.view addSubview:_tableView];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delaysContentTouches = NO;
        _tableView.tableFooterView = [[UIView alloc] init];
        [self.view addSubview:_tableView];
        
    }
    return _tableView;
}

-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark ----------UITableViewDelegate and UITableViewDataSource -----------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _data.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"myCell"];
    cell.textLabel.font = mAKT_Font_14;
    NSDictionary *dic = _data[indexPath.row];
    CBPeripheral *per = dic[@"peripheral"];
    cell.textLabel.text = [NSString stringWithFormat:@"%@-信号:%@",per.name,dic[@"RSSI"]];
    cell.tintColor = mAKT_Color_Color(0x1D,0xD0,0x79,1.0);
    cell.textLabel.font = mAKT_Font_14;
    cell.tintColor = mAKT_Color_Color(0x1D, 0xD0, 0x79,1.0);
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = _data[indexPath.row];
    CBPeripheral *per = dic[@"peripheral"];
    NSString *shoe = dic[@"shoe"];
    UITableViewCell *tableViewCell = [self.tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    if ([shoe isEqualToString:@"left"]) {
        [ANTBleFrameInstance connectUnbindLeftPeripheralWithDuration:5 peripheral:per success:nil failure:nil];
    }else
    {
         [ANTBleFrameInstance connectUnbindRightPeripheralWithDuration:5 peripheral:per success:nil failure:nil];
    }
}


@end
