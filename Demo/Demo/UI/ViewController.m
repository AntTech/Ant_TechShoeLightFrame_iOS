//
//  ViewController.m
//  ShoeLight
//
//  Created by 包月兴 on 2016/11/4.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import "ViewController.h"
#import "NSTimer+ANTBlockSupport.h"
#import "AKTKit.h"
#import "ANTBleFrame.h"
#import "ANTModeViewController.h"
#import "ANTDeviceViewController.h"
#import "RSColorPickerView.h"
@interface ViewController ()<ANTModeViewControllerDelegate,RSColorPickerViewDelegate>
@property (nonatomic,copy) UIImageView *backgroundImg;
@property (nonatomic,copy) UIImageView *stepImageView;
@property (nonatomic,copy) UIImageView *mileageImageView;
@property (nonatomic,copy) UIImageView *calImageView;

@property (nonatomic,copy) UILabel *stepLabel;
@property (nonatomic,copy) UILabel *mileageLabel;
@property (nonatomic,copy) UILabel *calLabel;

@property (nonatomic,copy) RSColorPickerView *colorPicker;

@property (nonatomic,strong) ANTSportMessage sportMessageBlock;
@end

@implementation ViewController
{
    NSTimer *timer_;
    NSInteger space_;
    
    ANTEffectMode effectMode_;
    ANTOperateMode operateMode_;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    space_ = (mAKT_SCREENWITTH - 28*3)/4;
    
    effectMode_ = ANTEffectModeOne;
    operateMode_ = ANTOperateModeTwoShoes;
    
    
    CGFloat x = (mAKT_SCREENWITTH - 280.0)/2.0;
    _colorPicker = [[RSColorPickerView alloc] initWithFrame:CGRectMake(x,64+30, 280.0, 280.0)];
     [_colorPicker setDelegate:self];
     _colorPicker.cropToCircle = YES;
    [self.view addSubview:_colorPicker];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UINavigationBar *nav = [[UINavigationBar alloc] initWithFrame:CGRectMake(0,20,mAKT_SCREENWITTH ,44)];
    UINavigationItem *navTitle = [[UINavigationItem alloc] initWithTitle:@"ANT幻彩鞋灯"];
    
    [nav pushNavigationItem:navTitle animated:YES];

    [self.view addSubview:nav];
    

    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:@"模式" style:UIBarButtonItemStylePlain target:self action:@selector(mode_presentViewController)];
    navTitle.leftBarButtonItem = leftItem;

    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"设备" style:UIBarButtonItemStylePlain target:self action:@selector(device_presentViewController)];
    navTitle.rightBarButtonItem = rightItem;

    _stepImageView = self.stepImageView;
    _stepLabel = self.stepLabel;
    _mileageImageView = self.mileageImageView;
    _mileageLabel = self.mileageLabel;
    _calImageView = self.calImageView;
    _calLabel = self.calLabel;
    
}

-(void)mode_presentViewController
{
    ANTModeViewController *modeVC = [[ANTModeViewController alloc] init];
    modeVC.delegate = self;
    modeVC.operateMode = operateMode_;
    modeVC.effectMode = effectMode_;
    [self presentViewController:modeVC animated:YES completion:nil];
}


-(void)device_presentViewController
{
    ANTDeviceViewController *deviceVC = [[ANTDeviceViewController alloc] init];
    [self presentViewController:deviceVC animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark  --- property 属性 ---



-(UIImageView *)stepImageView
{
    if (_stepImageView == nil) {
        _stepImageView = [UIImageView new];
        [self.view addSubview:_stepImageView];
        _stepImageView.image = mAKT_Image(@"icon_step");
        CGFloat y = _colorPicker.y + _colorPicker.height + 60;
        _stepImageView.frame =CGRectMake(space_,y,28,28);
        
    }
    return _stepImageView;
}



-(UIImageView *)mileageImageView
{
    if (_mileageImageView == nil) {
        _mileageImageView = [UIImageView new];
        [self.view addSubview:_mileageImageView];
        _mileageImageView.image = mAKT_Image(@"icon_mileage");
        CGFloat x = _stepImageView.x + _stepImageView.width + space_;
      _mileageImageView.frame =CGRectMake(x,_stepImageView.y,28,28);
        
    }
    return _mileageImageView;
}


-(UIImageView *)calImageView
{
    if (_calImageView == nil) {
        _calImageView = [UIImageView new];
        [self.view addSubview:_calImageView];
        _calImageView.image = mAKT_Image(@"icon_cal");
        CGFloat x = _mileageImageView.x + _mileageImageView.width + space_;
        _calImageView.frame =CGRectMake(x,_stepImageView.y,28,28);
    }
    return _calImageView;
}

-(UILabel *)stepLabel
{
    if (_stepLabel == nil) {
        CGFloat y = _stepImageView.y + _stepImageView.height + 5;
        CGFloat x = _stepImageView.x - 16;
        _stepLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,y,60,20)];
        [self.view addSubview:_stepLabel];
        _stepLabel.textAlignment = NSTextAlignmentCenter;
        _stepLabel.text = @"--";
        _stepLabel.textColor = mAKT_Color_LightGray;
    }
    return _stepLabel;
}


-(UILabel *)mileageLabel
{
    if (_mileageLabel == nil) {
        CGFloat x = _mileageImageView.x - 16;
        _mileageLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,_stepLabel.y,60,20)];
        [self.view addSubview:_mileageLabel];
        _mileageLabel.textAlignment = NSTextAlignmentCenter;
        _mileageLabel.text = @"--";
        _mileageLabel.textColor = mAKT_Color_LightGray;
        
    }
    return _mileageLabel;
}


-(UILabel *)calLabel
{
    if (_calLabel == nil) {
        CGFloat x = _calImageView.x - 16;
        _calLabel = [[UILabel alloc] initWithFrame:CGRectMake(x,_stepLabel.y,60,20)];
        [self.view addSubview:_calLabel];
        _calLabel.textAlignment = NSTextAlignmentCenter;
        _calLabel.text = @"--";
        _calLabel.textColor = mAKT_Color_LightGray;
    }
    return _calLabel;
}


-(ANTSportMessage)sportMessageBlock
{
    if (_sportMessageBlock == nil) {
        __weak __typeof(&*self)weakSelf = self;
        _sportMessageBlock = ^(NSInteger steps,float calorie,float mileage)
        {
            weakSelf.stepLabel.text = [NSString stringWithFormat:@"%ld",(long)steps];
            weakSelf.calLabel.text = [NSString stringWithFormat:@"%.2f",calorie];
            weakSelf.mileageLabel.text = [NSString stringWithFormat:@"%.2f",mileage];
        };
    }
    return _sportMessageBlock;
}


#pragma mark ---END---

#pragma mark - RSColorPickerView delegate methods -

- (void)colorPickerDidChangeSelection:(RSColorPickerView *)cp {
    
    [self p_closeTimer:timer_];
    timer_ = [NSTimer scheduledTimerWithTimeInterval:.1 block:^{
        
        CGFloat r, g, b, a;
        [[cp selectionColor] getRed:&r green:&g blue:&b alpha:&a];
        NSInteger ir = r * 255;
        NSInteger ig = g * 255;
        NSInteger ib = b * 255;
        [self p_sendDataWithEffectMode:effectMode_ operateMode:operateMode_ R:ir G:ig B:ib];
    } repeats:NO];
   

}
#pragma mark --------private Method-----------

#pragma mark --- 关闭定时器
-(void)p_closeTimer:(NSTimer *)timer
{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}


-(void)exchangeOperateMode:(ANTOperateMode)operateMode effectMode:(ANTEffectMode)effectMode
{
    effectMode_ = effectMode;
    operateMode_ = operateMode;
}




-(void)p_sendDataWithEffectMode:(ANTEffectMode)effectMode operateMode:(ANTOperateMode)operateMode R:(NSInteger)R G:(NSInteger)G B:(NSInteger)B
{
    switch (effectMode) {
        case ANTEffectModeOne:
        {
            [ANTBleFrameInstance syncLoveMonochromaticWithR:R G:G B:B operateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];
        }
            break;
        case ANTEffectModeTwo:
        {
            [ANTBleFrameInstance syncGorgeousMonochromaticWithR:R G:G B:B operateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];
            
        }
            break;
        case ANTEffectModeThree:
        {
            [ANTBleFrameInstance syncSlowMonochromaticWithR:R G:G B:B operateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];
        }
            break;
        case ANTEffectModeFour:
        {
            [ANTBleFrameInstance syncColorGradualWithOperateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];
        }
            break;
        case ANTEffectModeFive:
        {
            [ANTBleFrameInstance syncRibbonLawMonochromaticWithR:R G:G B:B operateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];
        }
            break;
        case ANTEffectModeSix:
        {
            [ANTBleFrameInstance syncColorfulRibbonLawWithOperateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];

        }
            break;
        case ANTEffectModeSeven:
        {
            [ANTBleFrameInstance syncHorselRibbonLawWithOperateMode:operateMode success:nil failure:^(ANTErorr *error) {
                
            }];
            
        }
            break;
            
        default:
            break;
    }
}




@end
