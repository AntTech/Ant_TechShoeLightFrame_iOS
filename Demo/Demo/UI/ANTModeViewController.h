//
//  ANTModeViewController.h
//  ShoeLight
//
//  Created by 包月兴 on 2016/11/9.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANTBleFrame.h"

@protocol ANTModeViewControllerDelegate <NSObject>

-(void)exchangeOperateMode:(ANTOperateMode)operateMode effectMode:(ANTEffectMode)effectMode;

@end


@interface ANTModeViewController : UIViewController
@property (nonatomic,assign) ANTOperateMode operateMode;
@property (nonatomic,assign) ANTEffectMode effectMode;
@property (nonatomic,weak) id<ANTModeViewControllerDelegate>delegate;
@end
