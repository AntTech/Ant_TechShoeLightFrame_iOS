//
//  ANTModeViewController.m
//  ShoeLight
//
//  Created by 包月兴 on 2016/11/9.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import "ANTModeViewController.h"
#import "AKTKit.h"
@interface ANTModeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,copy) UITableView *tableView;
@end

@implementation ANTModeViewController
{
    NSArray *colorModeArr_;
    NSArray *operateModeArr_;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    colorModeArr_ = @[@"恋恋单色模式",@"绚丽单色模式",@"慢热单色模式",@"五彩渐进模式",@"绚律单色模式",@"五彩绚律模式",@"鬼马绚律模式"];
    operateModeArr_ = @[@"双鞋模式",@"左鞋模式",@"右鞋模式"];
    self.view.backgroundColor = mAKT_Color_White;
    UINavigationBar *nav = [[UINavigationBar alloc] initWithFrame:CGRectMake(0,20,mAKT_SCREENWITTH ,44)];
    UINavigationItem *navTitle = [[UINavigationItem alloc] initWithTitle:@"模式"];
    [nav pushNavigationItem:navTitle animated:YES];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    navTitle.leftBarButtonItem = leftItem;
    [self.view addSubview:nav];
    _tableView = self.tableView;
}



-(void)cancel
{
    if (_delegate && [_delegate respondsToSelector:@selector(exchangeOperateMode:effectMode:)]) {
        [_delegate exchangeOperateMode:_operateMode effectMode:_effectMode];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,64,mAKT_SCREENWITTH, mAKT_SCREENHEIGHT) style:UITableViewStyleGrouped];
        [self.view addSubview:_tableView];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delaysContentTouches = NO;
        _tableView.tableFooterView = [[UIView alloc] init];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

#pragma mark ----------UITableViewDelegate and UITableViewDataSource -----------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 7;
    }else
    {
        return 3;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"myCell"];
    cell.textLabel.font = mAKT_Font_14;
    cell.tintColor = mAKT_Color_Color(0x1D,0xD0,0x79,1.0);
    switch (indexPath.section) {
        case 0:
        {
            cell.textLabel.text = colorModeArr_[indexPath.row];
            if (indexPath.row + 1 == _effectMode) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                 cell.textLabel.textColor= mAKT_Color_Color(0x1D,0xD0,0x79,1.0);
            }
        }
            break;
        case 1:
        {
            cell.textLabel.text = operateModeArr_[indexPath.row];
            if (indexPath.row == _operateMode) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                 cell.textLabel.textColor= mAKT_Color_Color(0x1D,0xD0,0x79,1.0);
            }
        }
            
        default:
            break;
    }
    cell.textLabel.font = mAKT_Font_14;
    cell.tintColor = mAKT_Color_Color(0x1D, 0xD0, 0x79,1.0);
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        _effectMode = indexPath.row + 1;
    }else
    {
        _operateMode = indexPath.row;
    }
    [_tableView reloadData];
}

- ( NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"炫彩模式";
    }else
    {
         return @"操作模式";
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
