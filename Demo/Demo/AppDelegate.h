//
//  AppDelegate.h
//  Demo
//
//  Created by 包月兴 on 2016/11/10.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

