//
//  ANTBleFrame.h
//  ShoeLight
//
//  Created by 包月兴 on 2016/11/4.
//  Copyright © 2016年 包月兴. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>


#pragma mark -----END----------------

#pragma mark --- define ---


#define ANTBleFrameInstance [ANTBleFrame sharedInstance]

#ifdef DEBUG // 调试
#define ANTLog(fmt, ...) {NSLog((@"%s [Line %d] DEBUG:--->" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);}
#else       // 发布打包
#define ANTLog(...)
#endif

#pragma mark --END---

#pragma mark -----枚举状态定义----------------
///性别
typedef NS_ENUM(NSInteger,ANTGenderType) {
    ///女
    ANTGenderTypeWoman = 1,
    ///男
    ANTGenderTypeMan,
};

///功能开关
typedef NS_ENUM(NSInteger,ANTFuntionSwitch){
    ///关闭
    ANTFuntionSwitchClose = 0,
    ///打开
    ANTFuntionSwitchOpen,
};

///操作模式
typedef NS_ENUM(NSInteger,ANTOperateMode){
    ///双鞋模式
    ANTOperateModeTwoShoes = 0,
    ///左鞋模式
    ANTOperateModeLeftShoes,
    ///右鞋模式
    ANTOperateModeRightShoes,
};

/// 炫彩模式
typedef NS_ENUM(NSInteger,ANTEffectMode){
    ANTEffectModeOne = 1,
    ANTEffectModeTwo,
    ANTEffectModeThree,
    ANTEffectModeFour,
    ANTEffectModeFive,
    ANTEffectModeSix,
    ANTEffectModeSeven,
};

///错误码
typedef NS_ENUM(NSInteger,ANTErrorCode) {
    ///不支持蓝牙设备
    ANTErrorCodeNoSupportBluetooth = 1,
    ///未打开蓝牙
    ANTErrorCodeBluetoothPoweredOff,
    ///蓝牙连接超时
    ANTErrorCodeBluetoothConnectTimeOut,
    ///蓝牙连接失败
    ANTErrorCodeBluetoothConnectFailure,
    ///写数据失败
    ANTErrorCodeWriteFailure,
    ///外设不存在
    ANTErrorCodeeNoPeripheral,
    
};


@interface ANTErorr : NSError

-(instancetype)initWithErrorCode:(ANTErrorCode)code;

@end

#pragma mark -----block定义----------------
///左鞋电量百分比
typedef void (^ANTLeftEctric)(NSInteger percent);
///右鞋电量百分比
typedef void (^ANTRightEctric)(NSInteger percent);
///运动详情(步数、消耗热量和里程)
typedef void (^ANTSportMessage)(NSInteger steps,float calorie,float mileage);
///失败信息
typedef void (^ANTBleFaliure)(ANTErorr *error);
///搜索外设
typedef void (^ANTSearchPeripheralArray)(NSArray *peripherals);


#pragma mark ---------END-----------




@interface ANTBleFrame : NSObject
@property (nonatomic,weak) ANTLeftEctric leftEctricBlock;
@property (nonatomic,weak) ANTRightEctric rightEctricBlock;
@property (nonatomic,weak) ANTSportMessage sportMessageBlock;
+ (instancetype)sharedInstance;
/**
 双鞋设备搜索

 @param duration 搜索时长
 @param success  扫描结束返回所有的外设数组(@[@{@”peripheral”: CBPeripheral对象,@”RSSI”:蓝牙信号强度,@"shoe":左右鞋标示}])
 @param failure  搜索失败
 */
#pragma mark --- 双鞋设备搜索
-(void)searchTwoPeripheralWithDuration:(NSInteger)duration
                               success:(ANTSearchPeripheralArray)success
                               failure:(ANTBleFaliure)failure;
/**
 左鞋设备搜索

 @param duration 搜索时长
 @param success  扫描结束返回所有的外设数组(@[@{@”peripheral”: CBPeripheral对象,@”RSSI”:蓝牙信号强度,@"shoe":左右鞋标示}])
 @param failure  搜索失败
 */
#pragma mark --- 左鞋设备搜索
-(void)searchLeftPeripheralWithDuration:(NSInteger)duration
                                success:(ANTSearchPeripheralArray)success
                                failure:(ANTBleFaliure)failure;


/**
 右鞋设备搜索

 @param duration 搜索时长
 @param success  扫描结束返回所有的外设数组(@[@{@”peripheral”: CBPeripheral对象,@”RSSI”:蓝牙信号强度,@"shoe":左右鞋标示}])
 @param failure  搜索失败
 */
#pragma mark --- 右鞋设备搜索
-(void)searchRightPeripheralWithDuration:(NSInteger)duration
                                 success:(ANTSearchPeripheralArray)success
                                 failure:(ANTBleFaliure)failure;


/**
 连接未绑定左设备

 @param duration   连接时长
 @param peripheral 左设备对象
 @param success    连接成功
 @param failure    连接失败
 */
#pragma mark --- 连接未绑定左设备
-(void)connectUnbindLeftPeripheralWithDuration:(NSInteger)duration
                                peripheral:(CBPeripheral *)peripheral
                                   success:(void(^)())success
                                   failure:(ANTBleFaliure)failure;


/**
 连接未绑定右设备

 @param duration   连接时长
 @param peripheral 右设备对象
 @param success    连接成功
 @param failure    连接失败
 */
#pragma mark --- 连接未绑定右设备
-(void)connectUnbindRightPeripheralWithDuration:(NSInteger)duration
                                    peripheral:(CBPeripheral *)peripheral
                                       success:(void(^)())success
                                       failure:(ANTBleFaliure)failure;
/**
 绑定左设备

 @param periperal 外设对象
 */
#pragma mark --- 绑定左设备
-(void)bindLeftPeriperal:(CBPeripheral *)periperal;


/**
 绑定右设备

 @param periperal 外设对象
 */
#pragma mark --- 绑定右设备
-(void)bindRightPeriperal:(CBPeripheral *)periperal;



/**
 连接已绑定左设备

 @param duration 连接时长
 @param success  连接成功
 @param failure  连接失败
 */
#pragma mark --- 连接已绑定左设备
-(void)connectBindedLeftPeriperalWithDuration:(NSInteger)duration
                                  success:(void(^)())success
                                  failure:(ANTBleFaliure)failure;



/**
 连接已绑定右设备

 @param duration 连接时长
 @param success  连接成功
 @param failure  连接失败
 */
#pragma mark --- 连接已绑定右设备
-(void)connectBindedRightPeriperalWithDuration:(NSInteger)duration
                                      success:(void(^)())success
                                      failure:(ANTBleFaliure)failure;

/**
 解绑左设备
 */
#pragma mark ---  解绑左设备
-(void)unbindLeftPeriperal;


/**
 解绑右设备
 */
#pragma mark ---  解绑右设备
-(void)unbindRightPeriperal;


/**
 同步炫彩---恋恋单色模式

 @param R           RGB R值大小
 @param G           RGB G值大小
 @param B           RGB B值大小
 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---  同步炫彩---恋恋单色模式
-(void)syncLoveMonochromaticWithR:(NSInteger)R
                                G:(NSInteger)G
                                B:(NSInteger)B
                      operateMode:(ANTOperateMode)operateMode
                          success:(void(^)())success
                          failure:(ANTBleFaliure)failure;

/**
 同步炫彩---绚丽单色模式
 
 @param R           RGB R值大小
 @param G           RGB G值大小
 @param B           RGB B值大小
 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---  同步炫彩---绚丽单色模式
-(void)syncGorgeousMonochromaticWithR:(NSInteger)R
                                    G:(NSInteger)G
                                    B:(NSInteger)B
                          operateMode:(ANTOperateMode)operateMode
                              success:(void(^)())success
                              failure:(ANTBleFaliure)failure;


/**
 同步炫彩---慢热单色模式
 
 @param R           RGB R值大小
 @param G           RGB G值大小
 @param B           RGB B值大小
 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---   同步炫彩---慢热单色模式
-(void)syncSlowMonochromaticWithR:(NSInteger)R
                                G:(NSInteger)G
                                B:(NSInteger)B
                      operateMode:(ANTOperateMode)operateMode
                          success:(void(^)())success
                          failure:(ANTBleFaliure)failure;


/**
同步炫彩---五彩渐进模式

 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---   同步炫彩---五彩渐进模式
-(void)syncColorGradualWithOperateMode:(ANTOperateMode)operateMode
                               success:(void(^)())success
                               failure:(ANTBleFaliure)failure;

/**
 同步炫彩---绚律单色模式

 @param R           RGB R值大小
 @param G           RGB G值大小
 @param B           RGB B值大小
 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---   同步炫彩---绚律单色模式
-(void)syncRibbonLawMonochromaticWithR:(NSInteger)R
                                     G:(NSInteger)G
                                     B:(NSInteger)B
                           operateMode:(ANTOperateMode)operateMode
                               success:(void(^)())success
                               failure:(ANTBleFaliure)failure;

/**
  同步炫彩---五彩绚律模式

 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---   同步炫彩---五彩绚律模式
-(void)syncColorfulRibbonLawWithOperateMode:(ANTOperateMode)operateMode
                                    success:(void(^)())success
                                    failure:(ANTBleFaliure)failure;

/**
 同步炫彩---鬼马绚律模式
 
 @param operateMode 操作模式
 @param success     成功
 @param failure     失败
 */
#pragma mark ---   同步炫彩---鬼马绚律模式
-(void)syncHorselRibbonLawWithOperateMode:(ANTOperateMode)operateMode
                                  success:(void(^)())success
                                  failure:(ANTBleFaliure)failure;


/**
 双鞋开关

 @param functionSwitch 开关
 @param success        成功
 @param failure        失败
 */
#pragma mark ---   双鞋开关
-(void)syncTwoSwitch:(ANTFuntionSwitch)functionSwitch
             success:(void(^)())success
             failure:(ANTBleFaliure)failure;


/**
 左鞋开关

 @param functionSwitch 开关
 @param success        成功
 @param failure        失败
 */
#pragma mark ---   左鞋开关
-(void)syncLeftSwitch:(ANTFuntionSwitch)functionSwitch
              success:(void(^)())success
              failure:(ANTBleFaliure)failure;

/**
 右鞋开关
 
 @param functionSwitch 开关
 @param success        成功
 @param failure        失败
 */
#pragma mark ---   右鞋开关
-(void)syncRightSwitch:(ANTFuntionSwitch)functionSwitch
               success:(void(^)())success
               failure:(ANTBleFaliure)failure;



@end
